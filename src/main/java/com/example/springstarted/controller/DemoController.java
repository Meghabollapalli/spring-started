package com.example.springstarted.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
/*
this controller or this annotation is example of component
component -> restcontroller, service, rpository
Rest-api crud operation
c -> create -> post/put
r -> read -> get
u -> update -> put/patch
d -> delete -> delete
Http methods
get method
put method
post method
delete method
patch method
 */
public class DemoController {

  @GetMapping
  public String returnHello(){
    return "this is my first spring project";
  }

  @GetMapping("/{name}") // <- accepting name as url and returning hello + name
  public String greetName(@PathVariable("name") String name){
    return "hello " + name;
  }

  @GetMapping("/{id}/{id2}") // localhost:8080/1/2
  public int add(@PathVariable("id") int id, @PathVariable("id2") int id2){
    return id + id2;
  }
}
