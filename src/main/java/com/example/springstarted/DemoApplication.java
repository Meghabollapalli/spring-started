package com.example.springstarted;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
/*
@configuration
@compoenentscan
@EnableautoConfiguration
client -> controller -> service -> repository -> service maps the data returned from db to model -> controller -> client
 */
public class DemoApplication {

	public static void main(String[] args) {

    SpringApplication.run(DemoApplication.class, args);
	}

}
